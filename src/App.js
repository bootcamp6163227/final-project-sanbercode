import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import LoginPage from "./Pages/LoginPage";
import HomePage from "./Pages/HomePage";
import TablePage from "./Pages/TablePage";
import FormPage from "./Pages/FormPage";
import UpdateFormPage from "./Pages/UpdateFormPages";
import ProductPage from "./Pages/ProductPage";
import RegisterPage from "./Pages/RegisterPage";
import ProfilePage from "./Pages/ProfilePage";
import ProtectedRoute from "./Wrapper/ProtectedRoute";
import GuestRoute from "./Wrapper/GuestRoute";
import DetailProductPage from "./Pages/DetailProductPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/detail/:articleId" element={<DetailProductPage />} />
          <Route path="/product" element={<ProductPage />} />

          <Route element={<GuestRoute />}>
            <Route path="/register" element={<RegisterPage />} />
            <Route path="/login" element={<LoginPage />} />
          </Route>

          <Route element={<ProtectedRoute />}>
            <Route path="/table" element={<TablePage />} />
            <Route path="/form" element={<FormPage />} />
            <Route path="/update/:articleId" element={<UpdateFormPage />} />
            <Route path="/profile" element={<ProfilePage />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
