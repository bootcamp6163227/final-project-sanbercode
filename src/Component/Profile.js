import axios from "axios";
import React, { useEffect, useState } from "react";

function Profile() {
  const [profile, setProfile] = useState({});

  const fetchProfile = async () => {
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/profile",
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      setProfile(response.data.data);
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };
  useEffect(() => {
    fetchProfile();
  }, []);

  return (
    <div>
      <section className="max-w-3xl mx-auto shadow-lg rounded-lg p-8 my-8">
        <h1 className="text-3xl text-center">Profile Pengguna</h1>

        {/* <!-- Form (Start) --> */}
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <p> Nama : {profile.name}</p>
          </div>
          <div className="col-span-5">
            <p> Username : {profile.username}</p>
          </div>
          <div className="col-span-5">
            <p> Email : {profile.email}</p>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Profile;
