import { Carousel } from "antd";
import React from "react";

const contentStyle = {
  margin: 0,
  height: "400px",
  color: "#fff",
  lineHeight: "480px",
  textAlign: "center",
  background: "#364d79",
};

function CarouselImage() {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };

  return (
    <Carousel afterChange={onChange}>
      <div>
        <h3 style={contentStyle}>
          <img
            src="https://api-project.amandemy.co.id/images/sepeda.jpg"
            className="w-full "
            alt
          />
        </h3>
      </div>
      <div>
        <h3 style={contentStyle}>
          <img
            src="https://api-project.amandemy.co.id/images/pisang.jpg"
            className="w-full object-cover"
            alt
          />
        </h3>
      </div>
    </Carousel>
  );
}

export default CarouselImage;
