import { Card } from "antd";
import React from "react";
import { Link } from "react-router-dom";

function Catalog({ catalog }) {
  return (
    <Card bordered={false} hoverable style={{ width: 305 }}>
      <Link to={`/detail/${catalog.id}`}>
        <div className="">
          <div className="w-64 gap-4">
            <img
              src={catalog.image_url}
              className="w-64 h-64 object-cover items-center rounded-lg"
              alt=""
            />
            <h1 className="text-stone-800 text-2xl">{catalog.name}</h1>
            {catalog.is_diskon === true ? (
              <div className="block mb-1 w-2/3">
                <p className="text-stone-800 text-lg line-through">
                  {catalog.harga_display}
                </p>
                <p className="text-stone-800 text-base">
                  {catalog.harga_diskon_display}
                </p>
              </div>
            ) : (
              <div>
                <p className="text-stone-800 text-lg">
                  {catalog.harga_display}
                </p>
              </div>
            )}
            <p className="text-stone-800 text-sm">Stock {catalog.stock}</p>
          </div>
        </div>
      </Link>
    </Card>
  );
}

export default Catalog;
