import { Button, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const validationSchema = Yup.object({
  email: Yup.string()
    .required("Email Pengguna Wajib Di Isi")
    .email("Format Email Tidak Valid"),
  password: Yup.string().typeError().required("Nama Artikel Wajib Di Isi"),
});

function LoginForm() {
  const navigate = useNavigate();
  const [input, setInput] = useState({
    email: "",
    password: "",
  });

  const handleCreate = async (values) => {
    console.log(values);
    try {
      // console.log("iseng aja");
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: values.email,
          password: values.password,
        }
      );
      // penyimpanan token
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("Username", response.data.data.user.username);
      alert("Berhasil Login");
      navigate("/table");
      // console.log(response.data.data);
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    }
  };

  const { handleChange, values, handleSubmit, errors, touched, handleBlur } =
    useFormik({
      initialValues: input,
      onSubmit: handleCreate,
      validationSchema: validationSchema,
    });

  return (
    <div>
      <section className="max-w-3xl mx-auto shadow-lg rounded-lg p-8 my-8">
        <h1 className="text-3xl">Login Pengguna</h1>

        {/* <!-- Form (Start) --> */}
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label>Email Pengguna</label>
            <Input
              onChange={handleChange}
              value={values.email}
              onBlur={handleBlur}
              name="email"
              type="text"
              placeholder="Masukan Email"
            />
            {touched.email === true && errors.email != null && (
              <p className="text-red-500 text-sm">{errors.email}</p>
            )}
          </div>
          <div className="col-span-5">
            <label>Password Pengguna</label>
            <Input.Password
              onChange={handleChange}
              value={values.password}
              onBlur={handleBlur}
              name="password"
              type="text"
              placeholder="Masukan Password"
            />
            {touched.password === true && errors.password != null && (
              <p className="text-red-500 text-sm">{errors.password}</p>
            )}
          </div>
        </div>

        <div className="flex justify-center gap-4 my-4">
          <Button htmlType="submit" type="primary" onClick={handleSubmit}>
            Login
          </Button>
        </div>
      </section>
    </div>
  );
}

export default LoginForm;
