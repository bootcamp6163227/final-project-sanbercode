import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import { useFormik } from "formik";
import { Link, useParams } from "react-router-dom";
import { Helmet } from "react-helmet";
import * as Yup from "yup";
import { Breadcrumb } from "antd";

function DetailProduct() {
  const { fetchCatalog, setCatalogDetail } = useContext(GlobalContext);
  const { articleId } = useParams();
  const [input, setInput] = useState({
    name: "", //Untuk Nama Barang
    stock: "", // Untuk Stock Barang
    harga: "", // Untuk Harga Barang
    is_diskon: false, // Untuk Status Diskon
    harga_diskon: "", // Untuk Harga Diskon
    image_url: "", // Untuk link gambar
    category: "", // Untuk Kategori Barang
    description: "", // Untuk Deskripsi
  });

  const fetchCatalogDetail = async () => {
    console.log();
    try {
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/final/products/${articleId}`
      );
      console.log(response.data.data); //Berhasil memanggil data dari database
      // setCatalogDetail(response.data.data);
      const article = response.data.data;
      console.log(input);
      // kita memasukan data dari server ke dalam form nya
      setInput(
        {
          name: article.name,
          harga: article.harga,
          stock: article.stock,
          image_url: article.image_url,
          is_diskon: article.is_diskon,
          harga_diskon: article.harga_diskon,
          category: article.category,
          description: article.description,
          harga_display: article.harga_display,
          harga_diskon_display: article.harga_diskon_display,
        },
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    }
  };

  const { handleChange, values, handleSubmit, errors, touched, handleBlur } =
    useFormik({
      initialValues: input,
      enableReinitialize: true,
    });

  useEffect(() => {
    fetchCatalogDetail();
  }, []);

  return (
    <div>
      <Helmet>
        <title>Detail Product ID : {articleId} </title>
      </Helmet>
      <section className="max-w-5xl mx-auto shadow-lg rounded-lg bg-gray-100 p-8 my-8">
        <Breadcrumb
          items={[
            {
              title: <Link to="/">Home</Link>,
            },
            {
              title: <Link to="/product">Product</Link>,
            },
            {
              title: "Detail",
            },
          ]}
        />
        {/* <!-- Form (Start) --> */}
        <div className="flex gap-x-6 gap-y-4 my-2 max-w-2xl mx-auto pt-7">
          <div className="">
            <img
              src={values.image_url}
              className="w-64 h-64 object-cover items-center shadow-sm rounded-lg mx-12"
              alt=""
            />
          </div>
          <div className="">
            <p className="text-3xl font-bold">{values.name}</p>
            <p className="text-base">{values.category}</p>
            <div>
              {values.is_diskon === true ? (
                <div className="block mb-1 w-2/3">
                  <p className="text-stone-800 text-lg line-through">
                    {values.harga_display}
                  </p>
                  <p className="text-red-800 text-base">
                    {values.harga_diskon_display}
                  </p>
                </div>
              ) : (
                <div>
                  <p className="text-stone-800 text-lg">
                    {values.harga_display}
                  </p>
                </div>
              )}
            </div>
            <p className="text-base">{values.description}</p>
            <p className="text-base">Stock {values.stock}</p>
          </div>
        </div>
      </section>
    </div>
  );
}

export default DetailProduct;
