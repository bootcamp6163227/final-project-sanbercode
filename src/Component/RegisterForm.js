import { Button, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const validationSchema = Yup.object({
  name: Yup.string().required("Nama Pengguna Wajib Di Isi"),
  username: Yup.string().required("username Pengguna Wajib Di Isi"),
  email: Yup.string()
    .required("Email Pengguna Wajib Di Isi")
    .email("Format Email Tidak Valid"),
  password: Yup.string().typeError().required("Nama Artikel Wajib Di Isi"),
  password_confirmation: Yup.string()
    .typeError()
    .required("Nama Artikel Wajib Di Isi"),
});

function RegisterForm() {
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
  });

  const handleCreate = async (values) => {
    console.log(values);
    try {
      // console.log("iseng aja");
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: values.name,
          username: values.username,
          email: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );

      alert("Berhasil Registasi");
      navigate("/login");
      // console.log(response.data.data);
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    }
  };

  const { handleChange, values, handleSubmit, errors, touched, handleBlur } =
    useFormik({
      initialValues: input,
      onSubmit: handleCreate,
      validationSchema: validationSchema,
    });

  return (
    <div>
      <section className="max-w-3xl mx-auto shadow-lg rounded-lg p-8 my-8">
        <h1 className="text-3xl">Registasi Pengguna</h1>

        {/* <!-- Form (Start) --> */}
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label>Nama Pengguna</label>
            <Input
              onChange={handleChange}
              value={values.name}
              onBlur={handleBlur}
              name="name"
              type="text"
              placeholder="Masukan Nama Barang"
            />
            {touched.name === true && errors.name != null && (
              <p className="text-red-500 text-sm">{errors.name}</p>
            )}
          </div>
          <div className="col-span-5">
            <label>Username Pengguna</label>
            <Input
              onChange={handleChange}
              value={values.username}
              onBlur={handleBlur}
              name="username"
              type="text"
              placeholder="Masukan Username"
            />
            {touched.username === true && errors.username != null && (
              <p className="text-red-500 text-sm">{errors.username}</p>
            )}
          </div>
          <div className="col-span-5">
            <label>Email Pengguna</label>
            <Input
              onChange={handleChange}
              value={values.email}
              onBlur={handleBlur}
              name="email"
              type="text"
              placeholder="Masukan Email"
            />
            {touched.email === true && errors.email != null && (
              <p className="text-red-500 text-sm">{errors.email}</p>
            )}
          </div>
          <div className="col-span-5">
            <label>Password Pengguna</label>
            <Input.Password
              onChange={handleChange}
              value={values.password}
              onBlur={handleBlur}
              name="password"
              type="text"
              placeholder="Masukan Password"
            />
            {touched.password === true && errors.password != null && (
              <p className="text-red-500 text-sm">{errors.password}</p>
            )}
          </div>
          <div className="col-span-5">
            <label>Konfirmasi Pengguna</label>
            <Input.Password
              onChange={handleChange}
              value={values.password_confirmation}
              onBlur={handleBlur}
              name="password_confirmation"
              type="text"
              placeholder="Masukan Konfirmasi Password"
            />
            {touched.password_confirmation === true &&
              errors.password_confirmation != null && (
                <p className="text-red-500 text-sm">
                  {errors.password_confirmation}
                </p>
              )}
          </div>
        </div>

        <div className="flex justify-center gap-4 my-4">
          <Button htmlType="submit" type="primary" onClick={handleSubmit}>
            Register
          </Button>
        </div>
      </section>
    </div>
  );
}

export default RegisterForm;
