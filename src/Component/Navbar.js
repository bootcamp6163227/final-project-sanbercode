import React from "react";
import { Link, useNavigate } from "react-router-dom";
import Logo from "./Logo/Logo.png";
import { Button } from "antd";
import axios from "axios";
function Navbar() {
  const navigate = useNavigate();
  const onLogout = async () => {
    try {
      // Melakukan Request Logout
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    } finally {
      // Menghapus Localstorage token & Username
      localStorage.removeItem("token");
      localStorage.removeItem("Username");
      alert("Berhasil Logout");
      navigate("/login");
    }
  };

  return (
    <header className=" shadow-lg py-2 bg-gray-100 px-9 sticky top-0 z-10">
      <nav className="max-w-7xl mx-auto flex justify-between py-4 px-12">
        <div className="">
          <Link to="/">
            <img src={Logo} className="px-9 w-10 h-15" />
          </Link>
        </div>
        <div className="justify-center">
          <ul className="flex gap-6 text-stone-800 text-xl list-none">
            <Link className="no-underline text-stone-800" to="/">
              <li>Home</li>
            </Link>
            <Link className="no-underline text-stone-800" to="/product">
              <li>Product</li>
            </Link>
            <Link className="no-underline text-stone-800" to="/table">
              <li>Table</li>
            </Link>
          </ul>
        </div>
        <div className="flex gap-2 items-center">
          {localStorage.getItem("token") == null ? (
            <>
              <Link to="/login">
                <Button type="primary">Login</Button>
              </Link>
              <Link to="/register">
                <Button type="default">Register</Button>
              </Link>
            </>
          ) : (
            <>
              <Link className="no-underline text-stone-800" to="/profile">
                <p className="text-lg text-stone-800 my-0">
                  Hai, {localStorage.getItem("Username")}
                </p>
              </Link>
              <Button onClick={onLogout} type="default" danger>
                Logout
              </Button>
            </>
          )}
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
