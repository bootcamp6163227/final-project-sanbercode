import axios from "axios";
import React, { useContext } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import { Link } from "react-router-dom";
import { Button, Input, Table } from "antd";
import { useState } from "react";

function TableAntd() {
  // const [filter, setFilter] = useState({
  //   category: "",
  //   search: "",
  // });
  // const [filterData, setFilterData] = useState([]);

  // const handleChange = (e) => {
  //   if (e.target.name === "search") {
  //     setFilter({
  //       ...filter,
  //       search: e.target.value,
  //     });
  //   } else if (e.target.name === "category") {
  //     setFilter({
  //       ...filter,
  //       category: e.target.value,
  //     });
  //   }
  // };
  // const handleSearch = () => {
  //   // console.log(filter);
  //   let tmpCatalog = structuredClone(catalog);

  //   if (filter.search !== "") {
  //     tmpCatalog = tmpCatalog.filter((catalog) => {
  //       return catalog.name.includes(filter.search);
  //     });
  //   }

  //   if (filter.category !== "") {
  //     tmpCatalog = tmpCatalog.filter((catalog) => {
  //       return catalog.category === filter.category;
  //     });
  //   }

  //   setFilterData(tmpCatalog);
  // };

  // const handleReset = () => {
  //   setFilter({
  //     Is_Diskon_applied: "",
  //     Search: "",
  //   });
  //   setFilterData(catalog);
  // };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Harga",
      dataIndex: "harga_display",
      key: "harga_display",
    },
    {
      title: "Harga Diskon",
      dataIndex: "harga_diskon_display",
      key: "harga_diskon_display",
    },
    {
      title: "Diskon berlaku/tidak",
      dataIndex: "is_diskon",
      key: "is_diskon",
      render: (text, record, index) => {
        if (record.is_diskon === true) {
          return <p>Diskon Berlaku</p>;
        } else {
          return <p>Diskon Tidak Berlaku</p>;
        }
      },
    },
    {
      title: "Gambar Artikel",
      dataIndex: "image_url",
      key: "image_url",
      render: (text, record, index) => {
        return <img className="w-64 " src={record.image_url} alt="" />;
      },
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2">
            <Link to={`/update/${record.id}`}>
              <Button type="primary">Update</Button>
            </Link>
            <Button type="primary" danger onClick={() => onDelete(record.id)}>
              Delete
            </Button>
          </div>
        );
      },
    },
  ];

  const { catalog, fetchCatalog, setEditForm } = useContext(GlobalContext);
  const onDelete = async (id) => {
    // Untuk kirim delete request
    try {
      let text = "Apakah Anda ingin menghapus data ?";
      if (window.confirm(text) === true) {
        const response = await axios.delete(
          `https://api-project.amandemy.co.id/api/final/products/${id}`,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );

        // alert("Berhasil menghapus data");
        fetchCatalog(catalog);
      } else {
        alert("Batal Menghapus Data");
      }
    } catch (error) {
      alert("Terjadi Error");
    }
  };

  // console.log(catalog);

  return (
    <div>
      <div className=" max-w-6xl mx-auto flex justify-end">
        {/* <div className="mb-6 flex gap-3 justify-end">
          <select
            onChange={handleChange}
            value={filter.category}
            id=""
            name="category"
            className="w-full py-1 px-2 rounded-md "
          >
            <option value="" disabled>
              Pilih Kategori
            </option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="hiburan">Hiburan</option>
            <option value="kendaraan">Kendaraan</option>
          </select>
          <Input
            name="search"
            placeholder="Search Article Name"
            type="text"
            onChange={handleChange}
            setValue={filter.search}
          />
          <Button type="primary" onClick={handleSearch}>
            Search
          </Button>
          <Button type="primary" danger onClick={handleReset}>
            Reset
          </Button>
        </div> */}
        <Link to={`/form`}>
          <Button type="primary"> Create Form +</Button>
        </Link>
      </div>
      <div className="max-w-6xl mx-auto w-full my-4">
        <Table dataSource={catalog} columns={columns}></Table>
      </div>
    </div>
  );
}

export default TableAntd;
