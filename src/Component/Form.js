import axios from "axios";
import React, { useContext, useState } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button } from "antd";

const validationSchema = Yup.object({
  name: Yup.string().typeError().required("Nama Artikel Wajib Di Isi"), //Untuk Nama Barang
  stock: Yup.number()
    .typeError("Angka Tidak Valid")
    .required("Stock Artikel Wajib Di Isi"), // Untuk Stock Barang
  harga: Yup.number()
    .typeError("Angka Tidak Valid")
    .required("Harga Artikel Wajib Di Isi"), // Untuk Harga Barang
  is_diskon: Yup.boolean(), // Untuk Status Diskon
  harga_diskon: Yup.number()
    .lessThan(Yup.ref("harga"), "Test")
    .typeError("Angka Tidak Valid")
    .when("is_diskon", {
      is: true,
      then: () =>
        Yup.number()
          .typeError("Angka Tidak Valid")
          .required("Harga Diskon Wajib diisi ketika diskon menyala"),
    }),
  image_url: Yup.string()
    .required("URL Image Wajib Di Isi")
    .url("Link URL Tidak Valid"), // Untuk link gambar
  category: Yup.string()
    .oneOf(["teknologi", "makanan", "minuman", "hiburan", "kendaraan"])
    .required("Kategori harus dipilih"), // Untuk Kategori Barang
  description: "", // Untuk Deskripsi
});

function Form() {
  const { fetchCatalog } = useContext(GlobalContext);
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "", //Untuk Nama Barang
    stock: 0, // Untuk Stock Barang
    harga: 0, // Untuk Harga Barang
    is_diskon: false, // Untuk Status Diskon
    harga_diskon: 0, // Untuk Harga Diskon
    image_url: "", // Untuk link gambar
    category: "", // Untuk Kategori Barang
    description: "", // Untuk Deskripsi
  });

  const handleCreate = async (values) => {
    console.log(values);
    console.log(validationSchema);
    try {
      console.log("iseng aja");
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          name: values.name,
          harga: values.harga,
          stock: values.stock,
          image_url: values.image_url,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          description: values.description,
        },
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      fetchCatalog();
      setInput({
        name: "", //Untuk Nama Barang
        stock: 0, // Untuk Stock Barang
        harga: 0, // Untuk Harga Barang
        is_diskon: false, // Untuk Status Diskon
        harga_diskon: 0, // Untuk Harga Diskon
        image_url: "", // Untuk link gambar
        category: "", // Untuk Kategori Barang
        description: "", // Untuk Deskripsi
      });
      alert("Form berhasil di Create");
      navigate("/table");
      // console.log(response.data.data);
    } catch (error) {
      // console.log(error.response.data.info);
      alert(error.response.data.info);
    }
  };

  const { handleChange, values, handleSubmit, errors, touched, handleBlur } =
    useFormik({
      initialValues: input,
      onSubmit: handleCreate,
      validationSchema: validationSchema,
    });

  return (
    <div>
      <section className="max-w-7xl mx-auto shadow-lg rounded-lg p-8 my-8">
        <h1 className="text-3xl">Form</h1>

        {/* <!-- Form (Start) --> */}
        <div className="grid grid-cols-2 gap-x-6 gap-y-4 my-2">
          <div className="block mb-1">
            <label>Nama Barang</label>
            <input
              onChange={handleChange}
              value={values.name}
              onBlur={handleBlur}
              name="name"
              type="text"
              placeholder="Masukan Nama Barang"
              className="block border border-zinc-600 bg-gray-100-100 rounded-md w-full px-2 py-1 text-sm"
            />
            {touched.name === true && errors.name != null && (
              <p className="text-red-500 text-sm">{errors.name}</p>
            )}
          </div>

          <div className="block mb-1">
            <label>Stock Barang</label>
            <input
              onChange={handleChange}
              value={values.stock}
              onBlur={handleBlur}
              name="stock"
              type="text"
              placeholder="Masukan Stock Barang"
              className="block border border-zinc-600 bg-gray-100 rounded-md w-full px-2 py-1 text-sm"
            />
            {touched.stock === true && errors.stock != null && (
              <p className="text-red-500 text-sm">{errors.stock}</p>
            )}
          </div>
        </div>

        <div className="flex gap-x-6 gap-y-4 my-2 align-text items-center">
          <div className="block mb-1 w-2/5">
            <label>Harga Barang</label>
            <input
              onChange={handleChange}
              value={values.harga}
              name="harga"
              onBlur={handleBlur}
              type="text"
              placeholder="Masukan Harga Barang"
              className="block border border-zinc-600 bg-gray-100 rounded-md w-full px-2 py-1 text-sm"
            />
            {touched.harga === true && errors.harga != null && (
              <p className="text-red-500 text-sm">{errors.harga}</p>
            )}
          </div>

          <div className="flex mb-1 place-items-center w-3/5 justify-content-center  ">
            <h3 className="w-1/3 gap-x-6 gap-y-4 my-2 text-center ">
              <input
                onChange={handleChange}
                checked={values.is_diskon}
                name="is_diskon"
                type="checkbox"
                className=""
              />
              Status Diskon
            </h3>
            {values.is_diskon === true ? (
              <div className="block mb-1 w-2/3">
                <label>Harga Diskon</label>
                <input
                  onChange={handleChange}
                  value={values.harga_diskon}
                  name="harga_diskon"
                  onBlur={handleBlur}
                  type="text"
                  placeholder="Masukan Harga Diskon"
                  className="block border border-zinc-600 bg-gray-100 rounded-md w-full px-2 py-1 text-sm"
                />
                {touched.harga_diskon === true &&
                  errors.harga_diskon != null && (
                    <p className="text-red-500 text-sm">
                      {errors.harga_diskon}
                    </p>
                  )}
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>

        <div className="flex gap-x-6 gap-y-4 my-2 align-text items-center">
          <div className="mb-1 w-1/3">
            <h3>Kategori Barang</h3>
            <select
              onChange={handleChange}
              value={values.category}
              onBlur={handleBlur}
              id=""
              name="category"
              className="w-full py-1 px-2 rounded-md bg-gray-100 border border-zinc-600 text-sm"
            >
              <option value="" disabled>
                Pilih Kategori
              </option>
              <option value="teknologi">Teknologi</option>
              <option value="makanan">Makanan</option>
              <option value="minuman">Minuman</option>
              <option value="hiburan">Hiburan</option>
              <option value="kendaraan">Kendaraan</option>
            </select>
            {touched.category === true && errors.category != null && (
              <p className="text-red-500 text-sm">{errors.category}</p>
            )}
          </div>
          <div className=" block mb-1 w-2/3">
            <h3>Gambar</h3>
            <input
              onChange={handleChange}
              value={values.image_url}
              type="text"
              onBlur={handleBlur}
              name="image_url"
              placeholder="Masukan Image URL"
              className="block border border-zinc-600 bg-gray-100 rounded-md w-full px-2 py-1 text-sm"
            />
            {touched.image_url === true && errors.image_url != null && (
              <p className="text-red-500 text-sm">{errors.image_url}</p>
            )}
          </div>
        </div>
        <div className="my-2">
          <h3>Deskripsi</h3>
          <textarea
            onChange={handleChange}
            name="description"
            type="text-area"
            className="block border border-zinc-600 bg-gray-100 rounded-md w-full px-2 py-1 text-sm h-32"
            value={values.description}
          ></textarea>
        </div>

        <div className="flex justify-end gap-4 my-4">
          <Link to="/table">
            <Button type="primary" danger>
              Cancel
            </Button>
          </Link>
          <Button type="primary" onClick={handleSubmit}>
            Create
          </Button>
        </div>
      </section>
    </div>
  );
}

export default Form;
