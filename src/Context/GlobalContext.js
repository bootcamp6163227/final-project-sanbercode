import { createContext, useState } from "react";
import axios from "axios";

export const GlobalContext = createContext();

export function ContextProvider({ children }) {
  const [catalog, setCatalog] = useState([]);
  const [catalogDetail, setCatalogDetail] = useState({});
  const [editForm, setEditForm] = useState(null);
  const fetchCatalog = async () => {
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/final/products"
      );
      // console.log(response.data.data); //Berhasil memanggil data dari database
      setCatalog(response.data.data);
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  return (
    <GlobalContext.Provider
      value={{
        catalog: catalog,
        setCatalog: setCatalog,
        fetchCatalog: fetchCatalog,
        editForm: editForm,
        setEditForm: setEditForm,
        catalogDetail: catalogDetail,
        setCatalogDetail: setCatalogDetail,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
}
