import React from "react";
import { Helmet } from "react-helmet";
import Navbar from "../Component/Navbar";
import Profile from "../Component/Profile";

function ProfilePage() {
  return (
    <div>
      <Helmet>
        <title>Profile Pages</title>
      </Helmet>
      <Navbar />
      <div className="mt-6">
        <Profile />
      </div>
    </div>
  );
}

export default ProfilePage;
