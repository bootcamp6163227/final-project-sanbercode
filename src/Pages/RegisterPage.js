import React from "react";
import { Helmet } from "react-helmet";
import Navbar from "../Component/Navbar";
import RegisterForm from "../Component/RegisterForm";

function RegisterPage() {
  return (
    <div>
      <Helmet>
        <title>Register Pages</title>
      </Helmet>
      <Navbar />
      <div className="mt-6">
        <RegisterForm />
      </div>
    </div>
  );
}

export default RegisterPage;
