import React from "react";
import Navbar from "../Component/Navbar";

import UpdateForm from "../Component/UpdateForm";

function UpdateFormPage() {
  return (
    <div>
      <Navbar />
      <div className="mt-6">
        <UpdateForm />
      </div>
    </div>
  );
}

export default UpdateFormPage;
