import React, { useContext, useEffect } from "react";
import Navbar from "../Component/Navbar";
import { GlobalContext } from "../Context/GlobalContext";
import Catalog from "../Component/Catalog";
import { Helmet } from "react-helmet";
import { Button, Carousel } from "antd";
import CarouselImage from "../Component/CarouselImage";
import { Link } from "react-router-dom";

function HomePage() {
  const { catalog, fetchCatalog, setEditForm, editForm } =
    useContext(GlobalContext);

  useEffect(() => {
    // console.log("test data..");
    fetchCatalog();
  }, []);
  return (
    <div>
      <Helmet>
        <title>Home Pages</title>
      </Helmet>
      <Navbar />
      <div>
        <div>
          <CarouselImage />
        </div>
        <div>
          <div className="flex max-w-7xl mx-auto justify-between items-center ">
            <h1 className="text-3xl text-center mb-8 mt-6">Articles</h1>
            <div>
              <Link to="/product">
                <Button type="default" className="content-center">
                  See More
                </Button>
              </Link>
            </div>
          </div>
          <div className=" max-w-7xl mx-auto grid grid-cols-4 gap-8 border p-6 flex gap-8 bg-gray-100 rounded-md shadow-lg">
            {catalog.map((catalog) => (
              <Catalog
                key={catalog.id}
                catalog={catalog}
                fetchCatalog={fetchCatalog}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomePage;
