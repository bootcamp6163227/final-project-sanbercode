import React from "react";
import { Helmet } from "react-helmet";
import Navbar from "../Component/Navbar";
import DetailProduct from "../Component/DetailProduct";

function DetailProductPage() {
  return (
    <div>
      <Helmet>
        <title> Product Detail Pages </title>
      </Helmet>

      <Navbar />

      <div className="mt-6">
        <DetailProduct />
      </div>
    </div>
  );
}

export default DetailProductPage;
