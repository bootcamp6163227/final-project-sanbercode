import React from "react";
import { Helmet } from "react-helmet";
import Navbar from "../Component/Navbar";
import LoginForm from "../Component/LoginForm";

function LoginPage() {
  return (
    <div>
      <Helmet>
        <title>Register Pages</title>
      </Helmet>
      <Navbar />
      <div className="mt-6">
        <LoginForm />
      </div>
    </div>
  );
}

export default LoginPage;
