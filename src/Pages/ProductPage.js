import React, { useContext, useEffect } from "react";
import Navbar from "../Component/Navbar";
import { Helmet } from "react-helmet";
import { GlobalContext } from "../Context/GlobalContext";
import Catalog from "../Component/Catalog";

function ProductPage() {
  const { catalog, fetchCatalog, setEditForm, editForm } =
    useContext(GlobalContext);

  useEffect(() => {
    // console.log("test data..");
    fetchCatalog();
  }, []);
  return (
    <div>
      <Helmet>
        <title>Home Pages</title>
      </Helmet>
      <Navbar />
      <div>
        <h1 className="text-3xl text-center mb-8 mt-6">Articles</h1>
        <div className=" max-w-7xl mx-auto grid grid-cols-4 gap-8 border p-6 flex gap-8 bg-gray-100 rounded-md shadow-lg">
          {catalog.map((catalog) => (
            <Catalog
              key={catalog.id}
              catalog={catalog}
              fetchCatalog={fetchCatalog}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default ProductPage;
