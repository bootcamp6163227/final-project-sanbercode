import React from "react";
import Navbar from "../Component/Navbar";
import { Helmet } from "react-helmet";
import Form from "../Component/Form";

function FormPage() {
  return (
    <div>
      <Helmet>
        <title>Form Pages</title>
      </Helmet>
      <Navbar />
      <div className="mt-6">
        <Form />
      </div>
    </div>
  );
}

export default FormPage;
