import React, { useContext, useEffect } from "react";
import Navbar from "../Component/Navbar";
import { GlobalContext } from "../Context/GlobalContext";
import { Helmet } from "react-helmet";
import TableAntd from "../Component/TableAntD";

function TablePage() {
  const { fetchCatalog } = useContext(GlobalContext);

  useEffect(() => {
    // console.log("test data..");
    fetchCatalog();
  }, []);

  return (
    <div>
      <Helmet>
        <title>Table Pages</title>
      </Helmet>
      <Navbar />
      <div className="mt-6">
        <TableAntd />
      </div>
    </div>
  );
}

export default TablePage;
