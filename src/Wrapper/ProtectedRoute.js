import React from "react";
import { Navigate, Outlet } from "react-router-dom";

//Mencegah user yang belum login ke supaya ke direct ke halaman login
function ProtectedRoute() {
  if (localStorage.getItem("token") == null) {
    return <Navigate to="/login" replace />;
  }

  return <Outlet />;
}

export default ProtectedRoute;
