import axios from "axios";
import React, { useContext, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import {  useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";

const validationSchema = Yup.object({
  name: Yup.string().required("Nama Artikel Wajib Diisi"),
  stock: Yup.number().required("Stock wajib diisi"), 
  harga: Yup.number().required("Harga wajib diisi"), 
  is_diskon: Yup.boolean(), 
  harga_diskon: Yup.number().required("Harga Diskon wajib diisi"), 
  image_url: Yup.string().required("Url gambar Wajib Diisi").url("format url tidak valid"), 
  category: Yup.string().oneOf(["teknologi", "makanan", "minuman", "hiburan", "kendaraan"]).required("Kategori harus dipilih"),
  description: Yup.string().required("Keterangan Wajib Diisi"),
})

function Form() {
  const {fetchArticles, articles} = useContext(GlobalContext);
  const Navigate = useNavigate()
  const [input, setInput] = useState({
    name: "", //Untuk Nama Barang
    stock: 0, // Untuk Stock Barang
    harga: 0, // Untuk Harga Barang
    is_diskon: false, // Untuk Status Diskon
    harga_diskon: 0, // Untuk Harga Diskon
    image_url: "", // Untuk link gambar
    category: "", // Untuk Kategori Barang
    description: "", // Untuk Deskripsi
  });


  

  const onSubmit = async (values) => {
    console.log(values);
   // console.log(input);
  // console.log("Menjalankan Post Request");
  //  try {
  //   const response = await axios.post("https://api-project.amandemy.co.id/api/products",
  //   {
  //     name: input.name,
  //     harga: input.harga,
  //     stock: input.stock,
  //     image_url: input.image_url,
  //     is_diskon: input.is_diskon,
  //     harga_diskon: input.harga_diskon,
  //     category: input.category,
  //     description: input.description
  // }
  //  );

  // fetchArticles();
  // setInput({
  //   name: "",
  //   harga: 0,
  //   image_url: "",
  //   stock: 0,
  //   harga_diskon: 0,
  //   category: "",
  //   description: "",
  //   is_diskon: false,
  // });
  // alert("berhasil diupload");
  // Navigate("/Three");
  //   // setArticles(response.data.data);
  // } catch (error) {
  //   alert(error.response.data.info);
  //   // console.log(error); 
  // }
  };

  const {handleChange, values, handleSubmit } = useFormik({initialValues: input,
    onSubmit: onSubmit,
  validationSchema: validationSchema });



  return (
    <div>
      <section className="max-w-6xl mx-auto shadow-lg rounded-lg bg-sky-100 p-7 my-7">
        <h1 className="text-3xl text-teal-700">Form</h1>

        {/* <!-- Form (Start) --> */}
        <div className="grid grid-cols-2 gap-x-6 gap-y-4 my-2">
          <div className="block mb-1">
            <label className="text-xl text-teal-900">Name</label>
            <input
              onChange={handleChange}
              value={values.name}
              name="name"
              type="text"
              placeholder="Masukan Nama Barang"
              className="block border border-zinc-600  rounded-md w-full px-2 py-1 text-sm"
            />
          </div>

          <div className="block mb-1">
            <label className="text-xl text-teal-900">Stock Barang</label>
            <input
              onChange={handleChange}
              name="stock"
              value={values.stock}
              type="number"
              placeholder="Masukan Stock Barang"
              className="block border border-zinc-600 rounded-md w-full px-2 py-1 text-sm"
            />
          </div>
        </div>

        <div className="flex gap-x-6 gap-y-4 my-2 align-text items-center">
          <div className="block mb-1 w-2/5">
            <label className="text-teal-900">Harga Barang</label>
            <input
              onChange={handleChange}
              name="harga"
              value={values.harga}
              type="number"
              placeholder="Masukan Harga Barang"
              className="block border border-zinc-600 rounded-md w-full px-2 py-1 text-sm"
            />
          </div>

          <div className=" flex mb-1 place-items-center w-3/5 justify-content-center text-center">
            <h3 className="w-1/3 gap-x-6 gap-y-4 my-2 text-center text-teal-900">
              <input
                onChange={handleChange}
                name="is_diskon"
                checked={values.is_diskon}
                type="checkbox"
                className=" "
              />
              Status Diskon
            </h3>
            {values.is_diskon === true ? (
            <div className="block mb-1 w-2/3 align-text">
            <label className="text-teal-900">Harga Diskon</label>
            <input
            onChange={handleChange}
            name="harga_diskon"
            value={values.harga_diskon}
            type="number"
            placeholder="Masukan Harga Diskon"
            className="block border border-zinc-600 rounded-md w-full px-2 py-1 text-sm"
            />
          </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>

        <div className="flex gap-x-6 gap-y-4 my-2 align-text items-center">
          <div className="mb-1 w-1/3">
            <h3 className="text-teal-900">Kategori Barang</h3>
            <select
              onChange={handleChange}
              name="category"
              value={values.category}
              className="w-full py-1 px-2 rounded-md border border-zinc-600 text-sm"
            >
              <option value="" disabled>
                Pilih Kategori
              </option>
              <option value= "teknologi">teknologi</option>
              <option value="makanan">Makanan</option>
              <option value="minuman">Minuman</option>
              <option value="hiburan">Hiburan</option>
              <option value="kendaraan">kendaraan</option>
            </select>
          </div>
          <div className=" block mb-1 w-2/3">
            <h3 className="text-teal-900">Gambar</h3>
            <input
              onChange={handleChange}
              type="text"
              name="image_url"
              value={values.image_url}
              placeholder="Masukan Image URL"
              className="block border border-zinc-600 rounded-md w-full px-2 py-1 text-sm"
            />
          </div>
        </div>
        <div className="my-2">
          <h3 className="text-teal-900">Deskripsi</h3>
          <textarea
            onChange={handleChange}
            name="description"
            value={values.description}
            type="text-area"
            className="block border border-zinc-600 rounded-md w-full px-2 py-1 text-sm h-32"
          ></textarea>
        </div>

        <div className="flex justify-end gap-4 my-4">
          <div className="button-form1">
            <button className="bg-sky-800 text-white border-blue-100 border-2 border px-4 py-1 rounded-lg">
              Cancel
            </button>
          </div>

          <div className="button-form2">
            <button
            type="submit" 
              onClick={handleSubmit}
              className="bg-sky-800 text-white border-2 border-blue-100 px-4 py-1 rounded-lg"
            >
              Create
            </button>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Form;